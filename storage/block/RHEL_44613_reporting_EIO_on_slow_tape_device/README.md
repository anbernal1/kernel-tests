# storage/block/RHEL_44613_reporting_EIO_on_slow_tape_device

Storage: kernel reporting EIO when add slow tape devices

## How to run it

Please refer to the top-level README.md for common dependencies.

### Install dependencies

```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test

```bash
bash ./runtest.sh
```
