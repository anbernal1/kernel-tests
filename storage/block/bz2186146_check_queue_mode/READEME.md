# storage/block/bz2186146_check_queue_mode

Storage: system get OOPs when queue mode is set to 1 from configfs

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
