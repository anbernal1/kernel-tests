# storage/ssd/nvme_pci_adapter_rescan_reset_remove_bz1370507

Storage: NVMe PCI adapter rescan reset remove 

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
#### You need run test_devs_setup to define TEST_DEVS
```bash
bash ./runtest.sh
```
